var node_env = process.env.NODE_ENV 
var config = require('./config/config')[node_env];
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schemaOptions = {
    versionKey: false,
    toObject: { getters: true },
    toJSON: { getters: true,  transform: function(doc, ret, options) {
        delete ret._id;
        delete ret.__v;
        return ret;
    }}
};

var ArticleSchema = new Schema({
    title: String,
    url: String,
    id: String,
    author: String,
    date: String,
    count: Number,
    send: Boolean,
    createTime: Date
}, schemaOptions);

ArticleSchema.path('createTime').get(function (time) {
    return time.getTime();
});

var UserSchema = new Schema({
    idfa: String,
    pushToken: String
}, schemaOptions);

module.exports = function () {
    mongoose.connect(config.db, function (err) {
        if (err)
            throw err;

        var db = mongoose.connection;
        var port = db.port;
        var host = db.host;
            
        console.log('config.db: ' + config.db);
        console.log('Successfully connected to MongoDB: ' + host + ':' + port);

        db.model('Article', ArticleSchema);
        db.model('User', UserSchema);
    });

    return mongoose.connection;
};