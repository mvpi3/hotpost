var cheerio = require('cheerio');
var request = require('request');
var async = require('async');
var debug = require('debug')('crawler')

var _pttBaseUrl = "https://www.ptt.cc";

//https://www.ptt.cc/bbs/Gossiping/M.1451969832.A.F26.html

function readBoardListAtIndexPage(boardName, index, callback) {
    var url = _pttBaseUrl + '/bbs/' + boardName;

    if (index <= 0) {
        url = url + "/index.html";
    } else {
        url = url + "/index" + index + ".html";
    }

    request({
        url: url,
        method: "GET",
        headers: {
            'cookie': 'over18=1;'
        }
    }, function (err, res, body) {

        if (err) {
            if (callback) {
                callback([], 0);
            }
            return;
        }

        var $ = cheerio.load(body);
        var pageBtns = $('a', '.pull-right');
        var lastPageUrl = $(pageBtns).eq(1).attr('href');
        var nextIndex = '';
        var articles = [];

        if (lastPageUrl) {
            var lastObj = lastPageUrl.split("index").pop();
            if (lastObj) {
                nextIndex = lastObj.replace(".html", "");
            }
        }

        $('.r-ent').each(function (index, elem) {

            if (elem == null || elem == undefined) {
                return false;
            }

            var title = $(this).find('a').text();
            var href = $(this).find('a').attr('href');
            var count = $(this).find('span').text();
            var author = $(this).find('.author').text();
            var date = $(this).find('.date').text();
            var id = '';

            if (href) {
                href = _pttBaseUrl + href;
                var lastObj = href.split("/").pop();
                if (lastObj) {
                    id = lastObj.replace(".html", "");
                }
            }

            var item = {
                title: title,
                url: href,
                count: count,
                author: author,
                date: date,
                id: id
            };

            var isAnnouncement = title.indexOf('公告') > -1;
            var isFinding = title.indexOf('協尋') > -1;

            if (title && href && id && !isAnnouncement && !isFinding) {
                articles.push(item);
                // console.log(item.title);
            }
        });

        if (callback) {
            callback(articles, nextIndex);
        }
        
        console.log("[Ptt] page %d done",index);
    });
}

function readBoardArticles(boardName, fromIndex, limitPageNumber, callback) {

    readBoardListAtIndexPage(boardName, fromIndex, function (articles, nextIndex) {
        var shouldGoNext = nextIndex && articles.length && limitPageNumber > 0;

        if (shouldGoNext) {
            setTimeout(function () {
                readBoardArticles(boardName, nextIndex, --limitPageNumber, function (nextPageArticles) {
                    callback(articles.concat(nextPageArticles));
                });
            }, 500);
        } else {
            callback(articles);
        }
    });
}

/*////////////////////////////////////////////////////////////////////
                        Public methods
////////////////////////////////////////////////////////////////////*/

exports.readGossiping = function (callback) {
    readBoardArticles('Gossiping', 0, 100, function (articles) {
        callback(articles);
    });
}
