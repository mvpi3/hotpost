var async = require('async');
var ptt = require('./pttcrawler');
var mongoose = require('./mongoose');
var Article = require('./model/article');
var CronJob = require('cron').CronJob;
var gcm = require('./gcmpusher');

module.exports.runCrawler = function () {
    var doing = function () {

        sendPush();

        ptt.readGossiping(function (articles) {
            Article.saveAndUpdate(articles, function (err) {
            });
        });
    }

    var done = function () {
        console.log('runCrawlerCronJob done');
    };

    /* Start the job right now */
    var job = new CronJob('0 */10 * * * *', doing, done, false, 'America/Los_Angeles');
    // var job = new CronJob('*/5 * * * * *', doing, done, true, 'America/Los_Angeles');

    job.start();
};

module.exports.runReomve = function () {

    var doing = function () {
        Article.deleteOlderOnes(3, function (err) {
        });
    };

    var done = function () {
        console.log('runReomveCronJob done');
    };

    /* run the job on sunday */
    var job = new CronJob('0 0 0 * * 0', doing, done, true, 'America/Los_Angeles');

    job.start();
}

function sendPush(callback) {
    async.waterfall([
        function (callback) {
            Article.recentHotPosts(40, 0, 200, function (err, docs) {
                docs = docs.filter(function (a) { return !a.send });
                callback(err, docs);
            });
        },
        function (docs, callback) {
            docs.forEach(function (elem) {
                // gcm.push();
                console.log("Sending GCM: " + elem.title);
                elem.send = true;
            });
            callback(null, docs);
        },
        function (docs, callback) {
            Article.saveAndUpdate(docs, function (err) {
                callback(err, 'done');
            });
        }
    ], function (err, result) {
        if (err) {
            console.log('[Error] Send PushNotification process is not compeleted. ' + err);
        } else if (result == 'done') {
            console.log('[Success] Send PushNotification process is compeleted.');
        } else {
            console.log('[Warnings] You should not see this.');
        }
    });
}