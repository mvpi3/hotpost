var Article = require('./model/article');
var User = require('./model/user');

module.exports.setRouter = function (app) {

    app.get('/', function (req, res) {
        Article.recentHotPosts(40, 0, 100, function (err, docs) {
            res.render('index', { article: docs });
        });
    })

    app.get('/recenthot', function (req, res) {
        var page = req.query.page || 0;
        Article.recentHotPosts(40, page, 20, function (err, docs) {
            if (err) {
                res.send({ 'status': 0, 'msg': err });
            } else {
                docs = docs.map(function (element) { return element.toJSON(); });
                res.send(docs);
            }
        });
    });

    app.get('/allusers', function (req, res) {
        User.getAll(function (err, docs) {
            res.send(docs);
        });
    });

    app.post('/user', function (req, res) {
        var idfa = req.body.idfa;
        var token = req.body.pushToken;

        if (!token) {
            res.send({ 'status': 0, 'msg': "no apn token" });
            return;
        }

        User.save(idfa, token, function (err, doc) {
            if (err) {
                res.send({ 'status': 0, 'msg': err });
            } else {
                res.send(doc);
            }
        });
    });

};