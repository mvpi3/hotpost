var express = require('express');
var morgan = require('morgan');
var path = require("path");
var compress = require('compression');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');

var async = require('async');
var ptt = require('./pttcrawler');
var mongoose = require('./mongoose');
var CronJob = require('./cronjob');
var Router = require('./router');

module.exports = function () {
    var app = express();
    var db = mongoose();
    
    // Set the application view engine and 'views' folder
    app.set('port', process.env.PORT);
    app.set('views', __dirname + '/views');
    app.set('view engine', 'ejs');

    // Set middleware 
    if (process.env.NODE_ENV === 'dev') {
        app.use(morgan('dev'));
    } else {
        app.use(compress());
    }

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(methodOverride());

    // Set routers 
    Router.setRouter(app);
    
    // Set Public folder and 404 file  
    app.use(express.static(__dirname + './public'));
    app.use(function (req, res) {
        res.status(404).send({ 'status': 0, 'msg': 'Page not found.' });
    });

    // run cron job  
    CronJob.runCrawler();
    CronJob.runReomve();

    return app;
};