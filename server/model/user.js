var Mongoose = require('mongoose');

///////////////////////
// Public Methods  //
///////////////////////

module.exports.save = function (idfa, pushToken, callback) {

    var UserModel = Mongoose.model('User');

    var query = { $or: [{ 'idfa': idfa }, { pushToken: pushToken }] };
    var options = { upsert: true, new: true };
    var object = {};
    if (idfa) object.idfa = idfa;
    if (pushToken) object.pushToken = pushToken;

    UserModel.findOneAndUpdate(query, object, options, function (err, doc) {
        if (callback)
            callback(err, doc);
    });
};

module.exports.getAll = function (callback) {
    var UserModel = Mongoose.model('User');
    UserModel.find({}, function (err, docs) {
        callback(err, docs);
    });
};
