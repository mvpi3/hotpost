var Mongoose = require('mongoose');
var async = require('async');

///////////////////////
// Private Methods  //
///////////////////////

function saveToDB(article, callback) {

    var ArticleModel = Mongoose.model('Article');

    var count = 0;
    if (Number.isInteger(article.count)) {
        count = article.count;
    } else if (article.count.indexOf('爆') > -1) {
        count = 100;
    } else if (article.count.indexOf('X') > -1) {
        count = -100;
    } else if (article.count) {
        count = Math.max(0, parseInt(article.count, 10));
    } else {
        count = 0;
    }

    var query = { id: article.id };
    var options = { upsert: false, new: false };
    var object = { count: count };
    if (article.send) {
        object.send = article.send;
    }

    ArticleModel.findOneAndUpdate(query, object, options, function (err, doc) {
        if (err) {
            if (callback)
                callback(err, doc);
            return;
        }

        if (doc) {
            if (callback)
                callback(err, doc);
            return;
        }

        var model = new ArticleModel({
            title: article.title,
            url: article.url,
            id: article.id,
            author: article.author,
            date: article.date,
            count: count,
            send: false,
            createTime: new Date()
        });

        model.save(function (err) {
            if (callback)
                callback(err, model);
        });
    });
}

///////////////////////
// Public Methods  //
///////////////////////

module.exports.saveAndUpdate = function (articles, callback) {

    async.eachSeries(articles, function (item, cb) {
        saveToDB(item, function (err, doc) {
            if (err) { console.log('[Error] saving ' + item.title); }
            // if (doc && !err) { console.log('[Success] saving ' + elem.title); }
            cb(err);
        });
    }, function (err) {
        // if (!err) { console.log('[Success] save all.'); }

        if (callback)
            callback(err);
    });
};

module.exports.deleteOlderOnes = function (beforeDays, callback) {

    var ArticleModel = Mongoose.model('Article');

    var oldDate = new Date();
    beforeDays = Math.max(beforeDays, 0);
    oldDate.setDate(oldDate.getDate() - beforeDays);

    var q = { 'createTime': { "$lt": oldDate } };

    ArticleModel.remove(q, function (err, removed) {
        if (err) {
            console.log('[Error] removing older post');
        } else {
            console.log('[Success] removed %d items', removed.result.n);
        }

        if (callback)
            callback(err);
    });
};


module.exports.recentHotPosts = function (hotLevel, page, limit, callback) {

    var ArticleModel = Mongoose.model('Article');
    limit = limit || 20;
    page = page > 0 ? page : 0;

    var afterDate = new Date();
    var days = 1;
    afterDate.setDate(afterDate.getDate() - days);


    var q = { 'count': { "$gte": hotLevel }, 'createTime': { "$gte": afterDate } , 'title': { "$not": /新聞/ } };
    var query = ArticleModel.find(q);
    query.limit(limit);
    query.skip(page * limit);
    query.sort({ count: 'desc' });

    query.exec(function (err, docs) {
        callback(err, docs);
    });
};
