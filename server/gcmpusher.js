
var node_env = process.env.NODE_ENV
var config = require('./config/config')[node_env];
var gcm_api_key = config.gcm_api_key;
var gcm = require('node-gcm');

function push(data, tokens) {
    var sender = new gcm.Sender(gcm_api_key);
    var message = new gcm.Message();

    for (var key in data) {
        if (data.hasOwnProperty(key)) {
            message.addData(key, data[key]);
        }
    }

    sender.send(message, { registrationTokens: tokens }, function (err, response) {
        if (err) {
            console.log("[gcm] Something has gone wrong! err :" + err);
        } else {
            console.log("[gcm] Sent with response: " + response);
        }
    });
}

module.exports.pushtest = function () {
    push({ 'title': 'alantest' }, ['894ee7186e7f20e65ada3ea011247e1f96bf85c13f8b5a50a675c78e1f41c6ad']);
}

module.exports.push = push;
