process.env.NODE_ENV = process.env.NODE_ENV || 'dev';
process.env.PORT = process.env.PORT || 3000;

var express = require('./express');
var app = express();

var server = app.listen(app.get('port'), function () {
    console.log('Server enviroment : ' + process.env.NODE_ENV);
    console.log('Server is listening at port: ' + app.get('port'));
});

module.exports = server;