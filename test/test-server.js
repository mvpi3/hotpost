var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/app');
var should = chai.should();
var expect = chai.expect;

chai.use(chaiHttp);

describe('APIs', function () {
    it('should fetch recent hot posts /recenthot GET', function (done) {
        chai.request(server)
            .get('/recenthot')
            .end(function (err, res) {
                res.should.be.json;
                res.should.have.status(200);
                res.should.not.be.empty;

                res.body.forEach(function (item) {
                    expect(item.count).to.least(40);
                });

                done();
            });
    });

    it('should fetch all users /allusers GET', function (done) {
        chai.request(server)
            .get('/allusers')
            .end(function (err, res) {
                res.should.be.json;
                res.should.have.status(200);
                res.should.not.be.empty;

                done();
            });
    });
});

